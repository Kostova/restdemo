module com.axway.rest {
    requires jersey.client;
    requires org.eclipse.jetty.server;
    requires org.eclipse.jetty.servlet;
    requires jersey.server;
    requires jersey.container.servlet.core;
    requires java.ws.rs;
    requires gson;
    requires jdk.incubator.httpclient;
    requires java.sql;
    requires jersey.common;
    exports com.axway.rest.demo.api;
    exports com.axway.rest.demo.entities;
}