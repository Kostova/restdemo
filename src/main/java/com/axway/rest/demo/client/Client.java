package com.axway.rest.demo.client;

import com.axway.rest.demo.entities.Result;
import com.google.gson.Gson;
import jdk.incubator.http.HttpClient;
import jdk.incubator.http.HttpRequest;
import jdk.incubator.http.HttpResponse;

import java.net.URI;

public class Client {
    public static void main(String[] args) throws Exception {
        var request = HttpRequest.newBuilder(URI.create("http://localhost:8080/calculator/squareRoot?input=5"))
                .GET()
                .build();
        var httpResponse = HttpClient.newHttpClient()
                .send(request, HttpResponse.BodyHandler.asString());
        var gson = new Gson();
        var result = gson.fromJson(httpResponse.body(), Result.class);
        System.out.println(result);
    }
}
