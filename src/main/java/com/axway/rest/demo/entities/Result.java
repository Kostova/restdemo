package com.axway.rest.demo.entities;

import java.io.Serializable;
import java.util.StringJoiner;

public class Result implements Serializable {

    double input;
    double output;
    String action;

    public Result(){}

    public Result(String action) {
        this.action = action;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public double getInput() {
        return input;
    }

    public void setInput(double input) {
        this.input = input;
    }

    public double getOutput() {
        return output;
    }

    public void setOutput(double output) {
        this.output = output;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Result.class.getSimpleName() + "[", "]")
                .add("input=" + input)
                .add("output=" + output)
                .add("action='" + action + "'")
                .toString();
    }
}
